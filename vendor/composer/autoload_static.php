<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitbea0d7cb9db94aa096d93212a0e99de8
{
    public static $prefixLengthsPsr4 = array (
        'Z' => 
        array (
            'Zoretow\\PhpTest\\' => 16,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Zoretow\\PhpTest\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitbea0d7cb9db94aa096d93212a0e99de8::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitbea0d7cb9db94aa096d93212a0e99de8::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitbea0d7cb9db94aa096d93212a0e99de8::$classMap;

        }, null, ClassLoader::class);
    }
}
